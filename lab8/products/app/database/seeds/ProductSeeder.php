<?php
class ProductSeeder extends Seeder {
    
    public function run()
    {
        Eloquent::unguard();
        $product = new Product;
        $product->name = 'Beer';
        $product->price = 3.00;
        $product->save();
        
        $product = new Product;
        $product->name = 'Bread';
        $product->price = 2.00;
        $product->save();
    }

}

?>
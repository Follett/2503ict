￼<html>
<head>
    <title>@yield('title')</title> </head>
<body>
￼@extends('layout') @section('title') Products
@stop @section('content')
<ul>
  @foreach ($products as $product)
    <li>{{{ $product->name }}}</li>
  @endforeach
</ul>
@stop
</body>
</html>
<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

/* Load sample data, an array of associative arrays. */
require "models/pms.php";


// Display search form
Route::get('/', function()
{
	return View::make('pms.query');
});

// Perform search and display results
Route::get('search', function()
{
  $name = Input::get('name');
  $year = Input::get('year');
  $state = Input::get('state');

  $results = search($name, $year, $state);

	return View::make('pms.results')->withPms($results);
});


/* Functions for PM database example. */

/* Search sample data for $name or $year or $state from form. */
function search($name, $year, $state) {

  // Filter $pms by $name
  if (!empty($name)) {
   $sql = "select * from pms where name  OR start  OR party Like ? OR state Like ?";
   $pms = DB::select($sql, array("%$name%","%$name%","%$name%","%$name%"));
  }
  return $pms;
}
<!-- display results -->
<!DOCTYPE html>
<!-- Results page of associative array search example. -->
<html>
<head>
    <title>@yield('title')</title>
    <meta charset="utf-8">
    <link rel="stylesheet" type="text/css" href="css/wp.css">
    <title>  @yield('title')</title>
</head>

<body>
  @yield('content')
  
</body>
</html>
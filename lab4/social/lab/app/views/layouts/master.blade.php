<head>
    
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Social media</title>
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <!-- Custom styles for this template -->
    <link href="css/styles.css" rel="stylesheet">
  
  </head>
  <body>
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
    <div class="container">
      <!-- Static navbar -->
      <nav class="navbar navbar-default">
        <div class="container-fluid">
          <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
            @foreach
            {{{ $results }}}
            <?php
              $temp = rand(1,10);
            ?>
            <a class="navbar-brand" href="#">Social Network <?php echo $temp ?></a>
          </div>
          <div id="navbar" class="navbar-collapse collapse">
            
            <ul class="nav navbar-nav navbar-right">
              <li><a href="#">Photos</a></li>
              <li><a href="#">Friends</a></li>
              <li><a href="#">Logon</a></li>
            </ul>
          </div><!--/.nav-collapse -->
        </div><!--/.container-fluid -->
      </nav>
      <!--Nav-->
      <div class="col-sm-3 col-sm-offset-0 blog-sidebar">
        <div class="sidebar-module-inset ">
          <h3>Jonathan Follett</h3>
          <img src="images/soccer-ball.png" alt="Soccer ball" style="width:180px;height:150px">
        </div>
        <br>
        <div class="sidebar-module-inset ">
          <h4>Friends Online</h4>
          <ul>
            <li>Adam</li>
            <li>Freeburge</li>
            <li>Perfecto</li>
            <li>Old mate</li>
            <li>Macbook overheatinf</li>
            <li>Kryptczych</li>
          </ul>
        </div>
      </div>
      
      <div class="col-sm-8 col-sm-offset-0">
      <?php 
          for ($x = 0; $x <= rand(1,10); $x++) { ?>
            <div class="blog-post">
              <p class="blog-post-meta"><?php echo $posts[$x][0]?> by Jonathan Follett<a href="#">    Like</a></p>
              <img src="images/<?php echo $posts[$x][2] ?>" alt="Soccer ball" class="float:left; img-responsive; width:180px;height:150px; ">
              <p style="align:left"><?php echo $posts[$x][1] ?></p>
              <hr>
            </div>
          
          
      
      
    </div>
    </div>
    @yield('content')
  </body>